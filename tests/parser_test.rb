require "minitest/autorun"

require_relative "../lib/nodes.rb"
require_relative "../lib/parser.rb"

class ParserTest < Minitest::Test

  def test_parser_generates_correctly
  code = <<-CODE
def method(a, b):
  true
CODE

  nodes = Nodes.new([
    DefNode.new("method", ["a", "b"],
      Nodes.new([TrueNode.new])
      )
    ])

    assert_equal nodes, ::Parser.new.parse(code)
  end
end
